//Item 3-4
let getCube = 2**3;
console.log(`The cube of 2 is ${getCube}`)


//Item 5-6
let address = [258, "Washington Ave NW", "California", 90011]

let [houseNumber,street,city,zip] = address;
console.log(`I live at ${houseNumber} ${street}, ${city} ${zip}`)



//Item 6-8
let animal = {
    animalName: "Lolong",
    type: "saltwater crocodile",
    weight: 1075,
    measurement: {
        ft: 20,
        in: 3
    }
}
let {animalName, type, weight, measurement} = animal
console.log(`${animalName} was a ${type}. He weighed at ${weight}kgs with a measurement of ${measurement.ft} ft ${measurement.in} in.` )


//Item 9-11
let number = [1,2,3,4,5];
number.forEach((num) => console.log(num))

const reduceNumber = number.reduce((a, b) => {
    return a + b;
})
console.log(reduceNumber)



//Item 12-13
class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const myDog = new Dog();
myDog.name = "Akamaru";
myDog.age = 10;
myDog.breed = "Aspin";
console.log(myDog);